# Launch Checklist
## Doc <head>
  1. Schema.org definition - Improves pages within Google Search

## Google Maps api
  1. Hide API key - https://console.developers.google.com/apis/credentials/key/288?project=ulp-landing-page&authuser=1

  https://support.google.com/googleapi/answer/6310037

## Contact form
  1. Enter the head's email and have them confirm the process

## PayPal.me
  1. Ask about having a link for someone else's so it doesn't say "Chris Johnson Medium," which could confuse people and create distrust.
