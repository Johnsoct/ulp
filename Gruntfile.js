module.exports = function(grunt) {
  grunt.initConfig({
    watch: {
      html: {
        files: 'src/srcindex.html',
        tasks: ['htmlmin']
      },
      css: {
        files: 'src/sass/*.sass',
        tasks: ['sass', 'postcss']
      },
    },
    htmlmin: {
      dist: {                                      // Target
        options: {                                 // Target options
          removeComments: true,
          collapseWhitespace: true
        },
        files: {                                   // Dictionary of files
          'index.html': 'src/srcindex.html'
        }
      }
    },
    sass: {
      dist: {
        options: {
          style: 'compressed',
          sourceMap: 'none'
        },
        files: [{
          src: ['src/styles.sass'],
          dest: 'dist/css/styles.css'
        }]
      }
    },
    postcss: {
      options: {
        map: false,
        processors: [
          require('pixrem')(),
          require('autoprefixer')({browsers:'last 2 versions'}),
          require('cssnano')()
        ]
      },
      dist: {
        src: 'dist/css/styles.css'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-sass');

  grunt.registerTask('w', ['watch']);
};
